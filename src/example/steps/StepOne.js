import React from 'react';
import '../steps.css'

const StepOne = () => {

		return (
				<section>
            <div className="text-center">
              <h5>ISI DATA</h5>
              <h6>Silahkan isi data diri anda</h6>
            </div>
            <div className="row">
              <div className="col-md-12 syarat info alert alert-warning">
              <h6 className='syarat-tes'> Syarat Tes CPNS : <br/> <br/>

              -SWAB ANTIGEN 1x24 Jam <br/>
              -PCR 2x24 Jam<br/> <br/>

              Surat harus dari rumah sakit / Klinik yang memiliki izin resmi sesuai standart terbaru KEMENKES. <br/>
              </h6>
              </div>
            </div>
						<div className="row">
								<div className="col-md-6">
										<div className="form-group">
												<label htmlFor="wfirstName2"> NIK :
														<span className="danger">*</span>
												</label>
												<input type="text" className="form-control required"
												       id="wfirstName2" name="firstName" maxLength="16" required /></div>
								</div>
								<div className="col-md-6">
										<div className="form-group">
												<label htmlFor="wlastName2"> Nama Lengkap :
														<span className="danger">*</span>
												</label>
												<input type="text" className="form-control required"
												       id="wlastName2" name="lastName" /></div>
								</div>
						</div>
						<div className="row">
            <div className="col-md-6">
										<div className="form-group">
												<label htmlFor="wdate2">Date of Birth :</label>
												<input type="date" className="form-control" id="wdate2" /></div>
								</div>
								<div className="col-md-6">
										<div className="form-group">
												<label htmlFor="numberPhone"> Nomor Handphone :
														<span className="danger">*</span>
												</label>
												<input type="number" className="form-control required"
												       id="number" name="number" /></div>
								</div>
						</div>
						<div className="row">
                <div className="col-md-6">
										<div className="form-group">
												<label htmlFor="alamat">Alamat :</label>
												<input type="tel" className="form-control" id="alamat" />
										</div>
								</div>
                <div className="col-md-6">
										<div className="form-group">
												<label htmlFor="email">Email :</label>
												<input type="email" className="form-control" id="email" />
										</div>
								</div>
              </div>
              <div className="row">
								<div className="col-md-6">
                  <div className="form-group">
                      <label htmlFor="wdate2">Tanggal Ujian :</label>
                      <input type="date" className="form-control" id="tanggalUjian" />
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="form-group">
                    <label htmlFor="wlocation2"> Pilih Sesi:
                        <span className="danger">*</span>
                    </label>
                    <select className="custom-select form-control required"
                            id="wlocation2" name="sesi">
                        <option value="">Select City</option>
                        <option value="one">1</option>
                        <option value="two">2</option>
                        <option value="three">3</option>
                        <option value="four">4</option>
                    </select>
                    </div>
                  </div>
              </div>
              <div className="row">
								<div className="col-md-12">
                  <div className="form-group">
                      <label htmlFor="upload-file">Foto KTP :</label>
                      <input type="file" className="form-control" id="myfile" />
                  </div>
                </div>
              </div>
				</section>
		);
};

export default StepOne;