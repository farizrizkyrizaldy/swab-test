import React from 'react';
import '../steps.css'

const StepThree = () => {
		return (
						<section>
              <div className="text-center">
                <h3>SELESAI</h3> 
                <h5>Selamat ,anda telah berhasil mendaftar!!</h5>
                <h6 style={{color: 'red'}}>*Diingatkan bagi para peserta harap melakukan swab H-1 sebelum hari ujian</h6>
              </div>
              <div className="col-md-12">
                <div className="alert alert-success">
                Data anda akan diproses oleh tim kami, kami akan mengirimkan email berupa invoice kepada anda (Bagi yang melakukan pembayaran non tunai). Bagi yang melakukan pembayaran tunai , kami akan memberikan invoice kepada anda setelah melakukan pembayaran. Terima Kasih
                </div>
              </div>
						</section>
		);
};

export default StepThree;