import React, { Component } from 'react';
import '../steps.css'

class StepTwo extends Component {

		state = {
				rekening: '',
				bank: '',
				pembayaran: '',
		};

		handleChangeInput = ( event ) => {
				this.setState( {
						[event.target.name]: event.target.value,
				} );
		};

		render() {
				return (
						<div>
								<section>
                  <div className="text-center">
                    <h4>PEMBAYARAN</h4>
                    <h5>Silahkan pilih metode pembayaran</h5>
                    <h6 style={{color: 'red'}}>No Rekening : 3790078631 (BCA) <br/>A.N Marsyella</h6>
                    <p>==================</p>
                    <input type="radio" id="transfer" name="fav_language" value="via_transfer" />
                    <label for="transfer">Via Transfer</label><br/>
                    <input type="radio" id="cod" name="fav_language" value="bayar_di_tempat" />
                    <label for="cod">Bayar Di Tempat</label><br></br>
                  </div>
										<div className="row">
												<div className="col-md-6">
														<div className="form-group">
																<label htmlFor="jobTitle2">Atas Nama :</label>
																<input type="text" className="form-control required"
																       name="rekening" onChange={ this.handleChangeInput }
																       value={ this.state.rekening } placeholder='Isi Nama Rekening'/>
														</div>
												</div>
												<div className="col-md-6">
														<div className="form-group">
																<label htmlFor="webUrl3">Nama Bank :</label>
																<input type="url" className="form-control required"
																       name="bank" onChange={ this.handleChangeInput }
																       value={ this.state.bank } placeholder='Isi Nama Bank'/></div>
												</div>
                        <div className="col-md-12">
                          <div className="form-group">
                              <label htmlFor="upload-file">Foto Bukti Pembayaran :</label>
                              <input type="file" className="form-control" id="myfile" name="pembayaran" onChange= { this.handleChangeInput } value={ this.state.pembayaran }/>
                          </div>
                        </div>
										</div>
								</section>
						</div>
				);
		}
}

export default StepTwo;