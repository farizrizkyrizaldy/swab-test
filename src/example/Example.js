import React, { Component } from 'react';

import StepOne from './steps/StepOne';
import StepTwo from './steps/StepTwo';
import StepThree from './steps/StepThree';
import './steps.css'

import StepWizard from '../wizard/StepWizard';

export default class Example extends Component {
		render() {

				const steps = [
						{ name: 'Isi Data', component: <StepOne />, preventNext: false },
						{ name: 'Pembayaran', component: <StepTwo />, preventNext: true },
						{ name: 'Selesai', component: <StepThree />, preventNext: true },
				];

				return (
				
				<div className={ 'page-wrapper' }>
					<h4 className={ 'title' }>PENDAFTARAN ANTIGEN PESERTA CPNS</h4>
						<div className={ 'container-fluid' }>
								<div className={ 'row' }>
										<div className={ 'col-12' }>
												<div className={ 'card' }>
														<div className={ 'card-body' }>
																<StepWizard
																		steps={ steps }
																		sendAction={ () => alert( 'Send' ) }
																		printAction={ () => alert( 'Print' ) }
																/>
														</div>
												</div>
										</div>
								</div>
						</div>
				</div>
				);
		}
}