import React from 'react';
import './css/navbar.css'

export const Button = ( { cls, position, title, handle } ) => {

		const className = `${ cls } ${ position }`;
		return (
				<input className={ className }
				       value={ title }
				       type={ 'submit' }
				       onClick={ handle } />
		);
};

Button.propTypes = {};